### Development Notes

* Run `gradle clean kaptKotlin` so that the extension methods for accessing your typeclasses are generated by the kotlin annotation processor.
* The generated sources should be included in your sourceset, so that the generated extension classes are available in your client code.
