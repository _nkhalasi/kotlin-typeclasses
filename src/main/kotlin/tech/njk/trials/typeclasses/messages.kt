package tech.njk.trials.typeclasses.messages

import arrow.extension
import java.time.LocalDateTime


data class DeferredTask(val name: String)
interface Message
interface Command : Message
data class HelloCmd(val name: String) : Message {
  companion object
}
data class BonjourCmd(val name: String): Message {
  companion object
}
interface Query : Message

interface Handler<T:Message> {
  fun T.handle(): DeferredTask
}

interface HelloCmdHandler : Handler<HelloCmd> {
  override fun HelloCmd.handle(): DeferredTask = DeferredTask("[${this.name}::${LocalDateTime.now()}]")
}
fun HelloCmd.Companion.handler(): Handler<HelloCmd> = object:HelloCmdHandler{}

@extension
interface BonjourCmdHandler : Handler<BonjourCmd> {
  override fun BonjourCmd.handle(): DeferredTask = DeferredTask("[${this.name}::${LocalDateTime.now()}]")
}
//fun BonjourCmd.Companion.handler(): Handler<BonjourCmd> = object:BonjourCmdHandler{}
