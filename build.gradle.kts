import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
  kotlin("jvm") version "1.3.21"
  kotlin("kapt") version "1.3.21"
  idea
}

group = "tech.njk.trials"
version = "1.0-SNAPSHOT"

val arrowVersion = "0.9.0"
val spek2Version = "2.0.0"
val junitVersion = "5.2.0"

repositories {
  mavenCentral()
  mavenLocal()
  jcenter()
//  maven(url = "https://oss.jfrog.org/artifactory/oss-snapshot-local/")
}

dependencies {
  runtime(kotlin("reflect"))
  compile("joda-time", "joda-time", "2.10.1")
  compile("ch.qos.logback", "logback-classic", "1.3.0-alpha4")
  compile("io.arrow-kt", "arrow-core-data", arrowVersion)
  compile("io.arrow-kt", "arrow-core-extensions", arrowVersion)
  compile("io.arrow-kt", "arrow-syntax", arrowVersion)
  compile("io.arrow-kt", "arrow-typeclasses", arrowVersion)
  compile("io.arrow-kt", "arrow-extras-data", arrowVersion)
  compile("io.arrow-kt", "arrow-extras-extensions", arrowVersion)
  compile("io.arrow-kt", "arrow-effects-extensions", arrowVersion)
  compile("io.arrow-kt", "arrow-effects-io-extensions", arrowVersion)
  compile("io.arrow-kt", "arrow-effects-rx2-data", arrowVersion)
  compile("io.arrow-kt", "arrow-effects-rx2-extensions", arrowVersion)
  compile("org.jetbrains.kotlinx", "kotlinx-coroutines-core", "1.1.1")

  kapt("io.arrow-kt", "arrow-meta", arrowVersion)

  testImplementation("org.junit.jupiter", "junit-jupiter-api", junitVersion)
  testImplementation("org.spekframework.spek2", "spek-dsl-jvm", spek2Version)
      .exclude("org.jetbrains.kotlin")
  testImplementation("org.amshove.kluent", "kluent", "1.47")
      .exclude("junit")
  testRuntimeOnly("org.junit.jupiter", "junit-jupiter-engine", junitVersion)
  testRuntimeOnly("org.spekframework.spek2", "spek-runner-junit5", spek2Version)
      .exclude("org.junit.platform")
      .exclude("org.jetbrains.kotlin")
}

tasks.withType<Test> {
  useJUnitPlatform {
    includeEngines("spek2")
  }
}

tasks.withType<KotlinCompile> {
  kotlinOptions.jvmTarget = "1.8"
}

sourceSets {
  main {
    java {
      srcDir("build/generated/sources/kaptKotlin/main/extension")
    }
  }
}

idea {
  module {
    sourceDirs.addAll(files(
        "build/generated/sources/kaptKotlin/main/extension"
    ))
    generatedSourceDirs.addAll(files(
        "build/generated/sources/kaptKotlin/main/extension"
    ))
  }
}