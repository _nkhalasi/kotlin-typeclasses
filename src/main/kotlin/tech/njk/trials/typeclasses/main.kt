package tech.njk.trials.typeclasses

import tech.njk.trials.typeclasses.messages.BonjourCmd
import tech.njk.trials.typeclasses.messages.HelloCmd
import tech.njk.trials.typeclasses.messages.bonjourcmd.handler.handler
import tech.njk.trials.typeclasses.messages.handler
import tech.njk.trials.typeclasses.user.User
import tech.njk.trials.typeclasses.user.user.eq.eq

fun main() {
  HelloCmd.handler().run {
    println(HelloCmd("hello").handle())
    println(HelloCmd("h e l l o").handle())
  }

  BonjourCmd.handler().run {
    println(BonjourCmd("bonjour").handle())
    println(BonjourCmd("b o n j o u r").handle())
  }

  val user = User(1)
  println(User.eq().run {
    user.eqv(User(1))
  })

  println("----Done----")
}
