package tech.njk.trials.typeclasses.user

import arrow.extension
import arrow.typeclasses.Eq

data class User(val id: Int) {
  companion object
}

@extension
interface UserEq : Eq<User> {
  override fun User.eqv(b: User): Boolean = this.id == b.id
}
